import secrets
from coolname import generate_slug

# Data are sorted by descending order unless if "ascending" is specified
def get_sort_order(request):
    sorting = request.args.get('sort')
    if sorting != None and sorting == "ascending":
        return "ascending"
    return "descending"

def get_nbr(request):
    #TMP: Limit to 200 items
    return 200

    default_nbr = 50
    max_nbr = 100
    nbr = request.args.get('nbr')
    if nbr == None or nbr == "":
        return default_nbr

    # Make sure value can be converted into an integer
    try:
        nbr = int(nbr)
    except Exception:
        return default_nbr

    # Make sure nbr value is acceptable
    if nbr > max_nbr:
        nbr = max_nbr
    return nbr


def create_item_name():
    return generate_slug(3)


def create_auth_token():
    return secrets.token_hex(15)
