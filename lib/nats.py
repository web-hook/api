import os
import json
import logging
import nats

class NATS(object):
    NATS_URL = os.getenv('NATS_URL', 'nats://nats:4222')

    # Publish a payload on a given subject
    @staticmethod
    async def pub(subject, payload):
        client = None

        try:
            # Check if credential file is provided
            if os.path.exists("/nats/creds"):
                logging.debug('using /nats/creds file for authentication')
                client = await nats.connect(NATS.NATS_URL,
                                user_credentials="/nats/creds")
            else:
                logging.debug('no authentication file provided')
                client = await nats.connect(NATS.NATS_URL)

            # Publishing message
            await client.publish(subject, payload=json.dumps(payload).encode())

            # Close connection
            await client.close()

            logging.debug('{} published to {}'.format(payload, subject))
        except Exception as e:
            logging.debug("cannot publish payload {} to NATS".format(payload))
            logging.debug(e)