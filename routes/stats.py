import logging
from flask import jsonify
from lib.db import Database
from . import routes


@routes.route('/stats', methods=['GET'])
def stats():

    # Get last webhook created
    last_webhook = Database.find_one("webhooks", {}, sort=[("created_at", -1)])
    webhooks_last_created_at = "-"
    if last_webhook:
        webhooks_last_created_at = last_webhook.get('created_at')
        webhooks_last_created_at = webhooks_last_created_at.replace("T", " ")[:-6] + " GMT"
        logging.info('Last webhook was created at: {}'.format(webhooks_last_created_at))
    else:
        logging.info('No webhook found')

    # Get last data created
    last_data = Database.find_one("data", {}, sort=[("created_at", -1)])
    data_last_created_at = "-"
    if last_data:
        data_last_created_at = last_data.get('created_at')
        data_last_created_at = data_last_created_at.replace("T", " ")[:-13] + " GMT"
        logging.info('Last data was created at: {}'.format(data_last_created_at))
    else:
        logging.info('No data found')


    # Get other statistics items
    webhooks_nbr = Database.count("webhooks", {})
    history_items_nbr = Database.count("data", {})

    # Return stats
    return jsonify(
        {
            "stats": {
                "number_of_webhooks": webhooks_nbr,
                "webhooks_last_created_at": webhooks_last_created_at,
                "number_of_payloads_received": history_items_nbr,
                "payload_last_received": data_last_created_at
            }
        }
    )
