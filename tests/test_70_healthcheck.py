import json
import pytest
import logging
import time


@pytest.mark.run(order=70)
def test_healthcheck_creation(wait_for_api):
    request_session, api_url = wait_for_api
    res = request_session.get('{}/readyz'.format(api_url))
    assert res.status_code == 200
